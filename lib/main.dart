import 'dart:math';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  int _savedNumber;
  int _randomNumber;

  @override
  void initState() {
    super.initState();
    _savedNumber = 0;
    _randomNumber = 0;
  }

  void _generateRandomNumber() {
    setState(() {
      _randomNumber = Random().nextInt(pow(2, 31));
    });
  }

  void _saveNumber() async {
    final SharedPreferences prefs = await _prefs;
    prefs.setInt('savedNumber', _randomNumber);
    setState(() {
      _savedNumber = _randomNumber;
    });
  }

  void _loadNumber() async {
    final SharedPreferences prefs = await _prefs;
    final savedNumber = prefs.getInt('savedNumber') ?? 0;
    setState(() {
      _randomNumber = savedNumber;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'You have generated random number:',
              ),
              Text(
                '$_randomNumber',
                style: Theme.of(context).textTheme.display2,
              ),
              Text(
                'You saved this number in SharedPreference',
              ),
              Text(
                '$_savedNumber',
                style: Theme.of(context).textTheme.display1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  OutlineButton(
                      onPressed: () => _loadNumber(), child: Text('Load')),
                  OutlineButton(
                      onPressed: () => _generateRandomNumber(),
                      child: Text('Random')),
                  OutlineButton(
                      onPressed: () => _saveNumber(), child: Text('Save')),
                ],
              ),
            ],
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}